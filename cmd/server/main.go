package main

import (
    "net/http"

    "github.com/yinzl/rpc/internal/haberdasherserver"
    "github.com/yinzl/rpc/haberdasher"
)

func main() {
  server := &haberdasherserver.Server{} // implements Haberdasher interface
  twirpHandler := haberdasher.NewHaberdasherServer(server, nil)

  http.ListenAndServe(":8080", twirpHandler)
}
