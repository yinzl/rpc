**RPC**
1. 比起 REST 和 GraphQL，RPC 有什么优点和缺点？
    >   优点：
    >       1. 性能高
    >       2. 简单易于理解（因为调用时像调用本地方法一样）
    >       3. 网络负载小
    >       4. 便于拆分服务，使用RPC后可以将核心和公共的业务提取出来作为独立服务，然后服务之间通过RPC通信调用其他服务的方法
    >   缺点：
    >       1. 高耦合性（因为调用者必须对调用的远程方法足够了解）
    >       2. function explosion （因为RPC本身很简单、轻量，因此同一个方法可能会存在很多版本，造成方法巨多）
    >       3. 可发现性差

2. 在什么样的场景下适合使用 RPC？什么场景下不适合？
    >   RPC：
    >       1. Internal Micro Services API（此类API特点为消息密集型，相应的对性能要求高）
    >       2. Command or Action API（此类API面向动作和指令而不是面向资源或者数据）
    ---
    >   NO RPC：
    >       1. Management API（此类API关注对象和资源适用于REST）
    >       2. Data or Mobile API（此类API关注数据适用于GraphQL）

**API Gateway**
1. API Gateway 这种设计模式有什么显著的缺点？有什么解决方案？
    >   显著缺点：单点故障，gateway server发生故障会导致整个系统无法使用
    ---
    >   解决方案：双机热备份（两台服务器均部署相同的gateway server，提供相同的服务，一台挂了另一台可以接管）

2. 参考 Rome 的 master 分支，我们是如何组织自己的 GraphQL Schema 的？为何要这么拆分？有什么好处？
    >   一个根Schema文件定义通用的Query、Mutation方法和模型，另外每个微服务模块有一个Schema文件夹用来存放其独有的Schema文件
    ---
    >   这样拆分逻辑清晰，Schema文件非常有序，方便不同模块的开发人员查看、新增以及编辑对应模块的Schema文件，互不影响，降低冲突发生概率，提高团队协作开发效率。另外通过使用根Schema提高代码的复用性。

3. 我们的项目中有很多代码生成的部分，研究 [go generate](https://golang.org/pkg/cmd/go/internal/generate/)。如何让我们的代码生成自动化？
    >   在自动化生成的go文件里添加 `//go:generate command arg1 arg2` 注释。比如在 `resolver.go` 文件里添加 `//go:generate go run github.com/99designs/gqlgen` 注释来自动化生成gqlgen代码（生成新的 `resolver.go` 文件到其他文件夹中）。Twirp框架生成代码同理。

**案例**
参考[案例](https://outcrawl.com/go-graphql-gateway-microservices)。
1. 作者是如何拆分服务的？这对我们有什么启示？怎样的业务适合拆分，什么样的不适合？
    >   作者是按照数据来拆分服务的，`Account service` 对用户信息数据提供CURD方法;`Catalog service` 对产品信息数据提供CURD方法;`Order service` 对订单信息数据提供CURD方法;另外这三种数据在实际业务中都是需要提供专门的管理页面的，从这个业务角度来说也是拆分的理由。
    ---
    >   启示：拆分服务时，先对业务数据分类，根据数据类别拆分服务，对不同类别的业务数据对应不同微服务。
    ---
    >   适合：数据类别不同而且不同类别的数据存在一套独立的业务逻辑，这样的业务适合拆分。
    >   不适合：数据类别没有明显的分界线，而且数据之间的业务耦合性高的情况下不适合

2. Gateway GraphQL 的 Schema 定义和各个微服务的接口定义有无重合之处？
    >   有重合之处，Gateway GraphQL 的 Schema 定义的接口可以和微服务定义的RPC接口一致，也可以在接口中调用多个微服务RPC接口来实现业务。

3. 在实现 Gateway 的 resolver 时，一般每个 resolver 要做哪些事情？
    >   1. 将resolver的输入参数转换为RPC接口所需的入参（一般resolver的入参模型和RPC入参模型不一致）
    >   2. 调用RPC接口完成业务逻辑（可能需要调用多个RPC接口）
    >   3. 返回RPC接口返回值（返回结果的模型是一致的，所以不需要转换）

4. 如何在 Gateway 中管理各个微服务的客户端？
    >   通过ROME服务来说明，首先 `rpc_clients.go` 中定义了一个RPCClients结构体，此结构体的成员是各个微服务的Client，所有微服务的Client都要放到这里，同时提供RPCClients的初始化方法，初始化时会将所有的成员实例化，实例化时调用Twirp框架生成的 `New{{Service}}ProtobufClient` 方法实例化。然后在服务主入口 `main.go` 的main函数中实例化一个RPCClients并初始化。最后将实例化的RPCClients传给Resolver，于是所有的resolver就都可以调用RPC接口了。

**共用模型**
请看 [exmaple.proto](./example.proto) 和 [example.graphql](./example.graphql)
1. 请在此基础上定义额外的 RPC 服务，实现 last, before 的功能。
    >   在proto文件中 `service` 定义中添加 `rpc GetSuggestions2(GetSuggestionsRequest2) returns (SuggestionConnection);` 同时添加如下消息定义
    ```
    message GetSuggestionsRequest2 {
        int32 last = 1;
        string before = 2;
        SuggestType suggest_type = 3;
        string query = 4;
    }   
    ```
    >   以上就是对RPC接口的定义，另外只需在GraphQL接口中判断传入参数是 `last，before` 时就调用 `GetSuggestions2` 接口即可。

2. 在 1 的基础上，用伪代码实现 GraphQL suggestions 的 resolver，使得它能根据情况调用不同的 RPC。(last, before 和 first, after 两组参数，同时只会有一组被设定)
    ```go
    var result
    if last == 0 && before == "" {
        result = GetSuggestions(&GetSuggestionsRequest{
            First:       int32(*first),
            After:       *after,
            SuggestType: suggestType,
            Query:       query})
    }
    else {
        result = GetSuggestions2(&GetSuggestionsRequest2{
            Last:       int32(*first),
            Before:       *after,
            SuggestType: suggestType,
            Query:       query})
    }
    return result
    ```

3. 如何绑定模型？gqlgen 的配置文件应该怎么写？
    >   首先需要定义 GraphQL Schema，再通过 proto 文件定义 RPC 服务，定义时注意让**proto 中返回结果的结构体与对应的 GraphQL query/mutation 返回的结构体一致。**然后通过 proto 文件生成 RPC 代码，之后通过 Schema 文件生成 GraphQL 接口时，将模型绑定为 `.pb.go` 文件中的模型即可。gqlgen 的配置文件中模型绑定应该声明为生成好的 `.pb.go` 文件里的模型。

**proto Wrapper Values**
请看 [proto-gql](https://gitlab.com/momentum-valley/proto-gql)
1. 相比 proto2，proto3 中为何没有 required 和 optional 的概念？
    >   required会破坏protobuf的兼容性，protobuf的思想是允许添加/删除定义的字段，同时仍然向前/向后兼容。而添加和删除required字段是不能保证兼容的，所以删除了required。没有required的话，其相对的optional就也没有存在的必要了。

2. 由于我们会用到第三方 proto 文件，protoc 如何添加查找路径？
    >   通过 `-I` 或 `--proto_path` 参数指定路径

3. 我们也会有自己的通用 proto 模型，比如 EntityType 和 PageInfo 等等。如何管理这些模型？生成 .pb 文件时如何避免生成冗余的代码？参考谷歌的 protobuf wrappers，请提出一个方案。
    >   通用 proto 模型应该统一放到同一个通用的 proto 文件中，需要使用时通过 import 导入通用 proto 文件即可。
    ---
    >   通过将需要多次使用的模型变成通用模型来避免

4. 按照 proto-gql 中的类型定义，请自己实现几个 Maybe 类型的 Marshaler 和 Unmarshaler。
    ```go
    // MarshalMaybeBytes ...
    func MarshalMaybeBytes(s wrappers.BytesValue) graphql.Marshaler {
        return graphql.WriterFunc(func(w io.Writer) {
            _, _ = fmt.Fprintf(w, "%q", string(s.Value))
        })
    }

    // UnmarshalMaybeBytes ...
    func UnmarshalMaybeBytes(v interface{}) (wrappers.BytesValue, error) {
        switch v := v.(type) {
        case string:
            return wrappers.BytesValue{Value: []byte(v)}, nil
        case *string:
            return wrappers.BytesValue{Value: []byte(*v)}, nil
        case []byte:
            return wrappers.BytesValue{Value: v}, nil
        case json.RawMessage:
            return wrappers.BytesValue{Value: []byte(v)}, nil
        default:
            return wrappers.BytesValue{Value: []byte{}}, fmt.Errorf("%T is not a BytesValue", v)
        }
    }

    // MarshalMaybeInt32 ...
    func MarshalMaybeInt32(d wrappers.Int32Value) graphql.Marshaler {
        return graphql.WriterFunc(func(w io.Writer) {
            _, _ = w.Write([]byte(strconv.Itoa(int(d.Value))))
        })
    }

    // UnmarshalMaybeInt32 ...
    func UnmarshalMaybeInt32(v interface{}) (wrappers.Int32Value, error) {
        switch v := v.(type) {
        case int:
            return wrappers.Int32Value{Value: int32(v)}, nil
        case int32:
            return wrappers.Int32Value{Value: v}, nil
        case int64:
            return wrappers.Int32Value{Value: int32(v)}, nil
        case json.Number:
            i, err := v.Int64()
            return wrappers.Int32Value{Value: int32(v)}, nil
        default:
            return wrappers.Int32Value{Value: 0}, fmt.Errorf("%T is not a Int32Value", v)
        }
    }
    ```

5. 对比一下 gqlgen 生成的模型，和 protoc 生成的模型。体会一下，什么情况下需要用到指针？什么情况下不需要用到指针？为什么？
    >   需要区分“空值”和“默认值”的成员需要用指针，而不需要区分也就是没有“空值”只有“默认值”的情况下不需要用到指针。避免产生把“空值”当作“默认值”去计算的错误。

6. protoc 中生成的 Enum 类型也需要通过实现 gqlgen 的 Marshaler 和 Unmarshaler 才能绑定。如何实现？
    >   Marshaler 通过调用 proto 生成的对应 enum 的 `String()` 方法实现，Unmarshaler 通过将传入的字符串作为 key 值到 proto 生成的对应 enum 的名为 `{EnumType}_value` 的 map 里去获取枚举对应的int值，然后再转换为对应的枚举类型。