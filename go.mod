module github.com/yinzl/rpc

go 1.12

require (
	github.com/golang/protobuf v1.3.3
	github.com/pkg/errors v0.9.1 // indirect
	github.com/twitchtv/twirp v5.10.1+incompatible
)
